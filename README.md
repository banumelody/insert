Contoh Insert ke database
=========================

Yang harus kamu lakukan:

Pertama
-------
	
	Download file zip di menu link Downloads di samping,

Kedua
-----

	Ekstrak di htdocs kamu,

Ketiga
------

	Buka 'http://localhost/phpmyadmin/' di browser kamu

Keempat
-------

	Import 'form.sql' ke database mysql kamu

Kelima
------

	Edit 'connector.php' sesuai setingan mysql kamu

Keenam
------

	Coba buka 'http://localhost/insert/fakultas.html' di browser kamu
