<link rel="stylesheet" type="text/css" href="style.css">

<form class="horizontal" action="add_jurusan.php" method="post">
	<div class="control-group">
		<label class="control-label" for="fakultas">Fakultas</label>
		<div class="control">
			<select id='fakultas'>
				<?php
					include 'connector.php';
					$con = connect();

					$select = mysqli_query($con,'select * from fakultas');

					while ($row = mysqli_fetch_array($select)) {
						?> 
						<option value ='<?php echo $row['id']; ?>'><?php echo $row['name'];?></option>
						<?php
					}
				?>

			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="jurusan">Jurusan</label>
		<div class="control">
			<input type="text" id='jurusan' placeholder="Nama Jurusan" name="jurusan">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" ></label>
		<div class="control">
			<button>Submit</button>
		</div>
	</div>
</form>